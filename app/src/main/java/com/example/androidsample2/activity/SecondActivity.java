package com.example.androidsample2.activity;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.example.androidsample2.R;

public class SecondActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
    }
}
